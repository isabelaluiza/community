# Comunidade RIOS (English version at end of Brazilian Portuguese version)

Nesse projeto, organizamos as atividades e discussões da comunidade RIOS. Esse repositório será mantido bilíngue, em português do Brasil e inglês.

A medida que avançarmos em nossos trabalhos, acrescentaremos mais informações aqui.

# RIOS Community

In this project, we organize the activities and discussions of RIOS community. This repository will be kept bilingual, both in Brazilian Portuguese and English.

As we progress in our work, we will add more information here.